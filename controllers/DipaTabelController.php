<?php

namespace app\controllers;

use Yii;
use app\models\DipaTabel;
use yii\data\ActiveDataProvider;
//use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use DateTime;
use yii\db\Expression;

/**
 * DipaTabelController implements the CRUD actions for DipaTabel model.
 */
class DipaTabelController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DipaTabel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => DipaTabel::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DipaTabel model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DipaTabel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DipaTabel();

        if ($model->load(Yii::$app->request->post())) {
            //$tanggal_dummy = DateTime::createFromFormat('d-m-Y H:i-s', $model->tanggal_dipa);
           // $model->tanggal_dipa = $tanggal_dummy;
            // $model->tanggal_dipa = $tanggal_dummy->getTimeStamp();
            $nilai_dipa = Yii::$app->request->post('DipaTabel')['nilai_dipa'];
            $nilai_dipa = str_replace(".", "", $nilai_dipa);
            $model->nilai_dipa = $nilai_dipa;
            $model->save();
            
            return $this->redirect(['view', 'id' => $model->id_dipa]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DipaTabel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_dipa]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionTry()
    {
        $tryd = DipaTabel::find()->where(['kode_' => 0])->orderBy('userid')->count();
    }

    /**
     * Deletes an existing DipaTabel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function action123(){
        echo "testing 123 Controller";
        exit;
    }

    public function actionExcoba2()
    {
       //$this->findModel($id)->excoba();

        // $results = \Yii::$app->db->createCommand("SELECT kode_kegiatan,tanggal_dipa,nilai_dipa from dipa_tabel WHERE kode_kegiatan = :kode_kegiatan", [':kode_kegiatan'=> '2881' ]) -> queryAll();
        // print "<pre>";
        // print_r($results);
        // print "</pre>";

        // $results2 = \Yii::$app->db->createCommand("SELECT count(*) from dipa_tabel WHERE kode_kegiatan = :kode_kegiatan AND YEAR(dipa_tabel) = :YEAR(dipa_tabel)", ['kode_kegiatan'=>'2881','YEAR(dipa_tabel)'=>2018]) -> queryScalar();
        // print "<pre>";
        // print_r($results2);
        // print "</pre>";   

        // $results2 = \Yii::$app->db->createCommand("SELECT * from dipa_tabel") -> query();
        // // print "<pre>";
        // // print_r($results2);
        // // print "</pre>";        
        // while ($row = $results2->read()) {
        // //     print "<pre>";
        //      print_r($row);
        // // } 

        // return $this->redirect(['excoba',
        //    'row' => $row;
        // ]);
        // $dipatbl = DipaTabel::find()->all();
        // $dafKategori = ArrayHelper::map($dafKategori,'id','nama');

        

         $angka1 = 2881;
         $angka2 = 2882;
         $mont1 = 10;
         $mont2 = 11;
        

        // $query = DipaTabel::find()->where("month(tanggal_dipa) = ".$mont." and kode_kegiatan='".$angka."' and year(tanggal_dipa) = 2018");
         
         $sum1 = DipaTabel::find()->where("month(tanggal_dipa) = ".$mont1." and kode_kegiatan='".$angka1."' and year(tanggal_dipa) = 2018")->sum("nilai_dipa");
         $sum2 = DipaTabel::find()->where("month(tanggal_dipa) = ".$mont2." and kode_kegiatan='".$angka1."' and year(tanggal_dipa) = 2018")->sum("nilai_dipa");
         $sum3 = DipaTabel::find()->where("month(tanggal_dipa) = ".$mont1." and kode_kegiatan='".$angka2."' and year(tanggal_dipa) = 2018")->sum("nilai_dipa");
         $sum4 = DipaTabel::find()->where("month(tanggal_dipa) = ".$mont2." and kode_kegiatan='".$angka2."' and year(tanggal_dipa) = 2018")->sum("nilai_dipa");
         // $sum1 = DipaTabel::find()->where("month(tanggal_dipa) = ".$mont1." and kode_kegiatan='".$angka1."' and year(tanggal_dipa) = 2018");
         // $sum2 = DipaTabel::find()->where("month(tanggal_dipa) = ".$mont2." and kode_kegiatan='".$angka1."' and year(tanggal_dipa) = 2018");
         // $sum3 = DipaTabel::find()->where("month(tanggal_dipa) = ".$mont1." and kode_kegiatan='".$angka2."' and year(tanggal_dipa) = 2018");
         // $sum4 = DipaTabel::find()->where("month(tanggal_dipa) = ".$mont2." and kode_kegiatan='".$angka2."' and year(tanggal_dipa) = 2018");

        // $nil_dpt = DipaTabel::find()->SELECT([new Expression('SUM(nilai_dipa) as total_dipa')])->groupBy('kode_kegiatan') ->all();
        // // $query = $nil_dpt;
        // print "<pre>";
        // print_r($nil_dpt);
        // print "</pre>"; 

        return $this->render('excoba2', [
            'sum1' => $sum1,
            'sum2' => $sum2,
            'sum3' => $sum3,
            'sum4' => $sum4
        ]);
        
        //$nil_dpt = DipaTabel::find()->where(['kode_kegiatan' => '2882'])->sum('nilai_dipa');
        
        // $nil_dpt2 = DipaTabel::find()->SELECT(['kode_kegiatan','nilai_dipa'])->groupBy(['kode_kegiatan']);
        // print($nil_dpt2);

        // $sql = "SELECT SUM(nilai_dipa) AS total_dipa, kode_kegiatan 
        //         FROM dipa_tabel 
        //         GROUP BY kode_kegiatan";

        // $model = DipaTabel::findBySql($sql)->all();
        // print "<pre>";
        // print_r($model);
        // print "</pre>";
    }

    public function actionTry2(){


        return $this->render('try3');
    }


    /**
     * Finds the DipaTabel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DipaTabel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DipaTabel::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
