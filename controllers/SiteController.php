<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Pekerja;
use app\models\userForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    // Editan Punya NAD

    public function actionHelloWorld()
    {
        $hello = "Nadra";
        return $this->render('helloWorld', compact('hello'));
    }

    public function actionFormPekerja()
    {
        $model = new Pekerja;

        $dataPekerja = Pekerja::find()->all();
                    
        if ($model -> load(Yii::$app->request-> post()) && $model->validate()){
           $model -> save(); # fungsi ini digunakan untuk menyimpan data dari form bekerja.
           Yii::$app->session->setFlash('success','Data Berhasil Disimpan');
           #session flash nya.
           return $this -> redirect (['site/form-pekerja']);
           #masih kondisi get

        }
        return $this->render('formPekerja', compact('model','dataPekerja'));
    }

    public function actionUser()
    {
        $model = new UserForm;

        if($model->load(Yii::$app->request->post()) && $model -> validate())
        {
            Yii::$app->session->setFlash('success','You have entered the data correctly');
        }
            return $this->render('userForm',['model'=>$model]);
        
    }
}
