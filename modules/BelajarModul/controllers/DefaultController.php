<?php

namespace app\modules\BelajarModul\controllers;

use yii\web\Controller;

/**
 * Default controller for the `BelajarModul` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function action123(){
    	//return $this->render('index2');
    	echo "Hay My name is semangat";
    	exit;
    }

    public function actionAll(){
    	$results = \Yii::$app->db->createCommand('SELECT * FROM dipa_tabel')->queryAll();
    	print "<pre>";
    	print_r($results);
    	print "</pre>";
    	exit;
    }
}
