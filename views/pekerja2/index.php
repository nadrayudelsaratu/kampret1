<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use kartik\export\ExportMenu;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

// $this->title = 'Pekerja2s';
$title = 'Pekerja2s';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pekerja2-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Pekerja2', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php 
        $gridColumns = [
            'id',
            'nama',
            'alamat',
            'keterangan',            
        ];

        echo ExportMenu::widget([
            'dataProvider' => $dataProvider, // untuk download
            'columns' => $gridColumns,
            'filename' => $title,
            'exportConfig' =>[
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_CSV => false,
                ExportMenu::FORMAT_PDF => false,
            ],
            'columnBatchToggleSettings' => [
                'label' => 'Pilih Semua',
            ],
        ]);
    ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider, // untuk bikin tabel
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'nama',
            'alamat',
            'keterangan',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
