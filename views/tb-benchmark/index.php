<?php

use yii\helpers\Html;
use yii\grid\GridView;
//use yii\grid\ActionColumn; //kalau mau bikin pengaturannya cuma bisa edit atau delete doang

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tabel Benchmarks';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tb-benchmark-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tambahkan Tabel', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'framework',
            'nilai',
            'penggunaan',

            ['class' => 'yii\grid\ActionColumn','header'=>'Perlakuan'],
            //['class' => ActionColumn::className(),'template'=>'{delete}','header'=>'Hapus'],
            //Hanya bisa delete, atau hanya bisa update, atau hanya bisa lihat. 
            //Cari codingannya di internet yaah
        ],
    ]); ?>
</div>
