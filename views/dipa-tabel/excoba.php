<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\gridView;
use yii\data\ActiveDataProvider;



/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tabel DIPA Coba Juga';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dipa-tabel-excoba">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tambahkan DIPA', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => true,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id_dipa',
            [
            'attribute' => 'kode_kegiatan',
            'label' => 'Kode Kegiatan',
            'filter' => Html::textInput('kode_kegiatan',Yii::$app->request->get('kode_kegiatan'),['class'=>'form-control']),
           // 'contentOptions' => ['style' => 'width:180px;']
            ],

            [
            'attribute' => 'tanggal_dipa',
            'label' => 'Tanggal DIPA',
            'format' => ['date', 'php:d-m-Y '],
           // 'contentOptions' => ['style' => 'width:100px;']
            'filter' => Html::textInput('tanggal_dipa',Yii::$app->request->get('tanggal_dipa'),['class'=>'form-control']),
            ],

            [
            'attribute' => 'nilai_dipa',
            'label' => 'Nilai DIPA',
            'value' => function ($DipaTabel){ 
                return $DipaTabel->Buatrp();
            },
            'filter' => Html::textInput('nilai_dipa',Yii::$app->request->get('nilai_dipa'),['class'=>'form-control']),
            //'contentOptions' => ['style' => 'width:100px;']
            ],

            [
            'attribute' => 'nama_dipa',
            'label' => 'Nama Penginput DIPA',
          //  'contentOptions' => ['style' => 'width:100px;']
            'filter' => Html::textInput('nama_dipa',Yii::$app->request->get('nama_dipa'),['class'=>'form-control']),
            ],

            // [
            // 'attribute' => 'jenis_revisi',
            // 'label' => 'Jenis DIPA',
            // //'contentOptions' => ['style' => 'width:100px;']
            // ],

            //'kode_kegiatan',
            //'tanggal_dipa',
            //'nilai_dipa',
            //'nama_dipa', masih ragu, nama dipa diganti apa yaa
            //'jenis_revisi',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
