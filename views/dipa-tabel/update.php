<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DipaTabel */

$this->title = 'Update Dipa Tabel: ' . $model->id_dipa;
$this->params['breadcrumbs'][] = ['label' => 'Dipa Tabels', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_dipa, 'url' => ['view', 'id' => $model->id_dipa]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="dipa-tabel-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
