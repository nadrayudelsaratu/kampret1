<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\gridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tabel DIPA Verifikasi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dipa-tabel-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Tambahkan DIPA', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id_dipa',
            [
            'attribute' => 'kode_kegiatan',
            'label' => 'Kode Kegiatan',
           // 'contentOptions' => ['style' => 'width:180px;']
            ],

            [
            'attribute' => 'tanggal_dipa',
            'label' => 'Tanggal DIPA',
            'format' => ['date', 'php:d-m-Y '],
           // 'contentOptions' => ['style' => 'width:100px;']
            ],

            [
            'attribute' => 'nilai_dipa',
            'label' => 'Nilai DIPA',
            'value' => function ($DipaTabel){ 
                return $DipaTabel->Buatrp();
            }
            //'contentOptions' => ['style' => 'width:100px;']
            ],

            [
            'attribute' => 'nama_dipa',
            'label' => 'Nama Penginput DIPA',
          //  'contentOptions' => ['style' => 'width:100px;']
            ],

            // [
            // 'attribute' => 'jenis_revisi',
            // 'label' => 'Jenis DIPA',
            // //'contentOptions' => ['style' => 'width:100px;']
            // ],

            //'kode_kegiatan',
            //'tanggal_dipa',
            //'nilai_dipa',
            //'nama_dipa', masih ragu, nama dipa diganti apa yaa
            //'jenis_revisi',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
