<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Departments */

$this->title = 'Realisasi Anggaran';
$this->params['breadcrumbs'][] = ['label' => 'Realisasi', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="departments-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('try2', [
        'model' => $model,
    ]) ?>

</div>