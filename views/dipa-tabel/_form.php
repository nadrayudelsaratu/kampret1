<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\DipaTabel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dipa-tabel-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kode_kegiatan')->textInput(['maxlength' => true]) ?>

   <?= $form->field($model, 'tanggal_dipa')->widget(DatePicker::classname(), [
                        'name' => 'check_issue_date', 
                        // 'value' => date('d-M-Y', strtotime("+2 days")),
                        'options' => ['placeholder' => 'Pilih Tanggal'],
                        'pluginOptions' => [
                            //'type'=>DateControl::FORMAT_DATETIME,
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                            'autoclose' => true
                            ]
                        ]);
                    ?>

    <?= $form->field($model, 'nilai_dipa')->textInput(['maxlength' => true, 'id' => 'number']) ?>

    <?= $form->field($model, 'nama_dipa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jenis_revisi')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<script src="../themes/js/jquery-3.1.1.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
    $("#number").keyup(function(event) {

      // skip for arrow keys
      if(event.which >= 37 && event.which <= 40) return;

      // format number
      $(this).val(function(index, value) {
        return value
        .replace(/\D/g, "")
        .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
        ;
      });
    });
    });

</script>

