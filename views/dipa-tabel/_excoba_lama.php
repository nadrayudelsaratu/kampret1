<?php

use app\models\DipaTabel;
use yii\helpers\Html;
use yii\data\ActiveDataProvider;

//use yii\helpers\Url;
//use yii\widgets\DetailView;
?>

<div>
	
	<h1><?= Html::encode($this->title) ?></h1>
	<div class="row">
		<div class="col-xs-12" style="text-align: center">
			<label>
				<h4>
					<b>
						TABEL DIPA
					</b>
				</h4>
			</label>
		</div>
	</div>

	<table class="table table-striped" style="text-align: center">
		<tr>
			<th style="text-align: center">Kode Kegiatan</th>
			<th style="text-align: center">Tanggal DIPA</th>
			<th style="text-align: center">Nilai DIPA</th>
			<th style="text-align: center">Nama DIPA</th>	
		</tr>
		<?php
			foreach ($model as $models) {
			?>
		<tr>
			<td><?php echo $models['kode_kegiatan']; ?></td>
			<td><?php echo $models['tanggal_dipa']; ?></td>
			<td><?php echo $models['nilai_dipa']; ?></td>
			<td><?php echo $models['nama_dipa']; ?></td>
		</tr>

		<?php
			}
		?>
	</table>
</div>