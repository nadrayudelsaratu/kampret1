<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Departments */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="departments-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tahun')->dropDownList([ '2018' => 'Tahun 2018', '2019' => 'Tahun 2019'], 
                                                    ['prompt' => '-Pilih Tahun-']) ?>

    <?= $form->field($model, 'bulan')->dropDownList([ '01' => 'Januari', '02' => 'Februari',
                                                      '03' => 'Maret', '04' => 'April', 
                                                      '05' => 'Mei', '06' => 'Juni',
                                                      '07' => 'Juli', '08' => 'Agustus',
                                                      '09' => 'September', '10' => 'Oktober',
                                                      '11' => 'November', '12' => 'Desember'], ['prompt' => '-Pilih Bulan-']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
