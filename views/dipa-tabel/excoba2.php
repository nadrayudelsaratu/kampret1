<?php

use app\models\DipaTabel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Berkasmasuk */
?>

<div class="row">
	<div class="col-md-4">
		<table class="table table-bordered">
			<thead>
				<tr class="">
					<th>No</th>
					<th>Kode Kegiatan</th>
					<th>Oktober</th>
					<th>November</th>
				</tr>
			</thead>
			<tbody>
				<tr class="">
					<td align=left><?= 1; ?></td>          
	                <td>2881</td>
	                <td align=right><?=$sum1?></td>
	                <td align=right><?=$sum2?></td>
				</tr>
				<tr class="">
					<td align=left><?= 2; ?></td>          
	                <td>2882</td>
	                <td align=right><?=$sum3?></td>
	                <td align=right><?=$sum4?></td>
				</tr>
			</tbody>
		</table>
	</div>
	
</div>
