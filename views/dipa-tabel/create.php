<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DipaTabel */

$this->title = 'Buat DIPA';
$this->params['breadcrumbs'][] = ['label' => 'Dipa Tabels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dipa-tabel-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
