<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\DipaTabel */

$this->title = $model->id_dipa;
$this->params['breadcrumbs'][] = ['label' => 'Dipa Tabels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dipa-tabel-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_dipa], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_dipa], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_dipa',
            'kode_kegiatan',
            'tanggal_dipa',
            'nilai_dipa',
            'nama_dipa',
            'jenis_revisi',
        ],
    ]) ?>

</div>

<?php 
$tes = date($model->tanggal_dipa);
echo $tes; ?>

