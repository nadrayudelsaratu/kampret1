<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Post2 */

$this->title = 'Create Post2';
$this->params['breadcrumbs'][] = ['label' => 'Post2s', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post2-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
