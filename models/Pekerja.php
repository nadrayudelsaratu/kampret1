<?php
	namespace app\models;

	use yii\db\ActiveRecord;

	class Pekerja extends ActiveRecord{
		
		public function rules (){
			return [
				[['nama','jabatan','email','keterangan'], 'required'],
				['email','email'],
				['keterangan','string','max' => 150],

			];
		}

		public function dataJabatan(){
			return[
				1 => 'CEO',
				2 => 'COO',
				3 => 'Supervisor',
				4 => 'Staff Biasa'
			];
		}

		public function labelJabatan(){
			if($this->jabatan==1){
				return 'CEO';
			}elseif ($this->jabatan==2) {
				return 'COO';
			}elseif ($this->jabatan==3) {
				return 'Supervisor';
			}elseif ($this->jabatan==4) {
				return 'Staff Biasa';
			}else{
				return 'Unknown';
			}
		}
	}

