<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post2".
 *
 * @property int $post_id
 * @property string $post_title
 * @property string $post_description
 * @property int $author_id
 *
 * @property User2 $author
 */
class Post2 extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'post2';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['post_title', 'post_description', 'author_id'], 'required'],
            [['post_description'], 'string'],
            [['author_id'], 'integer'],
            [['post_title'], 'string', 'max' => 100],
            [['author_id'], 'integer'], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'post_id' => 'Post ID',
            'post_title' => 'Post Title',
            'post_description' => 'Post Description',
            'author_id' => 'Author ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User2::className(), ['user_id' => 'author_id']);
    }
}
