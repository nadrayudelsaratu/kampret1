<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pekerja2".
 *
 * @property int $id
 * @property string $nama
 * @property string $alamat
 * @property string $keterangan
 */
class Pekerja2 extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pekerja2';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'alamat', 'keterangan'], 'required'],
            [['nama'], 'string', 'max' => 50],
            [['alamat', 'keterangan'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'keterangan' => 'Keterangan',
        ];
    }
}
