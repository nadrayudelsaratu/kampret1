<?php

namespace app\models;

use Yii;
use app\models\EntryForm;

/**
 * This is the model class for table "dipa_tabel".
 *
 * @property int $id_dipa
 * @property string $kode_kegiatan
 * @property string $tanggal_dipa
 * @property string $nilai_dipa
 * @property string $nama_dipa
 * @property string $jenis_revisi
 */
class DipaTabel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dipa_tabel';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_kegiatan', 'nilai_dipa', 'nama_dipa', 'jenis_revisi'], 'required'],
            [['tanggal_dipa'], 'safe'],
            [['nilai_dipa'], 'number'],
            [['kode_kegiatan'], 'string', 'max' => 4],
            [['nama_dipa'], 'string', 'max' => 255],
            [['jenis_revisi'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_dipa' => 'Id Dipa',
            'kode_kegiatan' => 'Kode Kegiatan',
            'tanggal_dipa' => 'Tanggal Dipa',
            'nilai_dipa' => 'Nilai Dipa',
            'nama_dipa' => 'Nama Dipa',
            'jenis_revisi' => 'Jenis Revisi',
        ];
    }

    
    public function Buatrp(){
        $nilai_dipa =DipaTabel::findOne($this->id_dipa);
        $angka =$nilai_dipa->nilai_dipa; 
        $jadi = "Rp " . number_format($angka,0,',','.').",-";
        return $jadi;
    }

}
